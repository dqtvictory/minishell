NAME		=	minishell
CC			=	clang
CFLAGS		=	-Wall -Wextra -Werror
LFT			=	libft/libft.a
INC			=	-I ./inc
LIB			=	-L ./libft -lft -lreadline
OBJ			=	$(patsubst src%, obj%, $(SRC:.c=.o))
SRC			=	src/launcher/launch_wait_process.c \
				src/launcher/launch.c \
				src/launcher/execute.c \
				src/launcher/launch_reset_redirection.c \
				src/launcher/launch_builtin.c \
				src/launcher/launch_redirect.c \
				src/launcher/launch_heredoc.c \
				src/launcher/execute_path.c \
				src/launcher/execute_builtin.c \
				src/builtins/cd.c \
				src/builtins/exit.c \
				src/builtins/unset.c \
				src/builtins/echo.c \
				src/builtins/export_extra.c \
				src/builtins/export.c \
				src/builtins/pwd.c \
				src/builtins/env.c \
				src/envs/envp.c \
				src/envs/destroy.c \
				src/envs/init.c \
				src/envs/var.c \
				src/parser/split_line.c \
				src/parser/add_arg_to_args.c \
				src/parser/line.c \
				src/parser/command.c \
				src/parser/file.c \
				src/parser/expand_line_utils.c \
				src/parser/skip_quotes.c \
				src/parser/is_line_not_empty.c \
				src/parser/create_command.c \
				src/parser/expand_redirect_op.c \
				src/parser/handle_unclosed_quotes.c \
				src/parser/expand_line.c \
				src/parser/remove_quotes.c \
				src/parser/split_command.c \
				src/misc/die.c \
				src/misc/close_cmd_fd.c \
				src/misc/free_sstrs.c \
				src/misc/destroy_process.c \
				src/misc/line_read.c \
				src/misc/free_cmd.c \
				src/main.c

all:		$(LFT) obj $(NAME)

$(NAME):	$(OBJ)
			$(CC) $(CFLAGS) -o $@ $^ $(LIB)

$(LFT):		
			@echo " [ .. ] | Compiling libft.."
			@make -sC libft
			@echo " [ OK ] | Libft ready!"

obj:
			@mkdir -p obj
			@mkdir obj/launcher
			@mkdir obj/builtins
			@mkdir obj/envs
			@mkdir obj/parser
			@mkdir obj/misc

obj/%.o:	src/%.c
			$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
			@make $@ -sC libft
			@rm -rf $(OBJ) obj
			@echo "clean: object files removed."

fclean:		clean
			@make $@ -sC libft
			@rm -rf $(NAME)
			@echo "fclean: binary files removed."

re:			fclean all

.PHONY:		all obj clean fclean re
