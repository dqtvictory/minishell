/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 15:36:02 by qdam              #+#    #+#             */
/*   Updated: 2021/08/01 15:36:02 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "includes.h"
# include "structs.h"

extern volatile t_data	g_data;

////	FILE HANDLER	////

void	close_cmd_fd(t_cmd *cmd, size_t nb_cmd);
void	p_error(char *name, char *errmsg, int errnum);

////	PROCESS LAUNCHER	////

int		wait_process(t_cmd *cmd, size_t nb_cmd);
void	destroy_process(t_cmd *cmd, size_t nb_cmd, t_var **env,
			int exit_status);
int		launch(t_cmd *cmd, t_var **env, int exit_status);
int		launch_builtin(t_cmd *cmd, size_t index_cmd, t_var **env,
			int exit_status);
void	execute(t_cmd *cmd, size_t index_cmd, t_var **env, int exit_status);
int		execute_builtin(t_cmd *cmd, size_t index_cmd, t_var **env,
			int exit_status);
int		execute_path(t_cmd *cmd, size_t index_cmd, t_var *env, char **envp);

////	LINE READER		////

char	*line_read(char *line, char *prompt);
t_cmd	*parse_line(char **line, t_var *env);
int		is_line_not_empty(char *line_expand);
t_cmd	*create_command(size_t nb_cmd, char ***command_redirect);
char	**split_line(char *line_expand);
char	*expand_line(char *line_content, t_var *env, int exit_status);

////	COMMAND PARSER	////

int		parse_command(t_cmd *cmd, size_t nb_cmd, char ***command_redirect,
			t_var *env);
int		parse_infile(t_cmd *cmd, size_t i, size_t *index_args,
			char ***command_redirect);
int		parse_outfile(t_cmd *cmd, size_t i, size_t *index_args,
			char ***command_redirect);
char	***split_command(char **command_list, size_t nb_cmd);
char	*handle_unclosed_quotes(char **line);
void	skip_quotes(char *command, size_t *i);
int		remove_quotes(char **command);
char	**expand_redirect_op(char **command_list, size_t nb_cmd);
char	**add_arg_to_args(char **input_args, char *arg);

////	REDIRECTIONS	////

int		heredoc(char *delimiter, t_var *env, int exit_status);
int		redirect(t_cmd *cmd, size_t index_cmd, t_var **env);
int		reset_redirection(t_cmd *cmd, size_t index_cmd);

////	ENVIRONMENT VARIABLES	////

void	destroy_var(t_var *var);
t_bool	check_var_name(char *name);
t_var	*make_var(char *name, char *value);
t_var	*construct_globals(char **envp);
t_bool	add_var(t_var **list, char *name, char *value);
void	del_var(t_var **list, char *name);
char	*get_var(t_var *list, char *name);
t_bool	set_var(t_var *list, char *name, char *new_value);
char	**export_to_envp(t_var *globals);
t_bool	check_var_exist(t_var *list, char *name);

////	SHELL BUILTINS	////

int		mini_cd(char **args, t_var *var_list);
int		mini_echo(char **args);
int		mini_env(char **args, t_var *var_list);
int		mini_exit(char **args, t_var **var_list_ptr, int exit_status,
			t_bool one_cmd);
int		mini_export(char **args, t_var **var_list_ptr);
int		mini_pwd(char **args, t_var *var_list);
int		mini_unset(char **args, t_var **var_list_ptr);

////	FREE MEMORY		////

void	free_cmd(t_cmd *cmd, size_t nb_cmd);
void	free_sstrs(char ***command_redirect, size_t nb_cmd);
void	clear_list(t_var **list);

#endif