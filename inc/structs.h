/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 15:37:54 by qdam              #+#    #+#             */
/*   Updated: 2021/08/01 15:43:01 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_H
# define STRUCTS_H

# include "includes.h"

typedef struct s_data	t_data;
typedef struct s_cmd	t_cmd;
typedef struct s_var	t_var;
typedef struct s_node	t_node;

struct s_data
{
	int		exit_status;
	int		is_execution;
};

struct s_cmd
{
	size_t	nb_cmd;
	size_t	index_cmd;
	char	**args;
	char	**redirection;
	char	**redirect_op;
	int		fd_in;
	int		fd_out;
	int		save_stdin;
	int		save_stdout;
	pid_t	pid_process;
	int		exit_status;
};

struct s_var
{
	char	*name;
	char	*value;
	t_var	*next;
};

struct s_node
{
	char	c[2];
	char	*s;
	t_bool	free_s;
	size_t	len;
	t_node	*next;
};

#endif
