/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   defines.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 15:36:24 by qdam              #+#    #+#             */
/*   Updated: 2021/08/01 15:36:59 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_H
# define DEFINES_H

# define RESET			"\033[0m"
# define YELLOW			"\033[33m"
# define MAGENTA		"\033[35m"
# define CYAN			"\033[36m"

# define PROGRAM_NAME	"minishell"
# define PROMPT			"\033[35m§¬ \033[36mminishell-42 \033[33mŸ \033[0m"
# define DEFAULT_PATH	"/usr/local/sbin:/usr/local/bin: \
						/usr/sbin:/usr/bin:/sbin:/bin"

# define EMPTY_STR		""

# define EXIT_ARG_ERROR	": numeric argument required"
# define EXIT_MANY_ARGS	"too many arguments"
# define ENV_NAME_ERROR	"': not a valid identifier"
# define CD_ARG_ERROR	"too many arguments"
# define CD_HOME_ERROR	"HOME not set"

#endif
